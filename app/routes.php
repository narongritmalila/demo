<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	//return View::make('hello');
});

Route::get('/', array('as' => 'home', 'uses' => 'HomeController@index')); // root
Route::get('post/create', array('as' => 'post.create', 'uses' => 'HomeController@create'));
Route::post('post/save', array('as' => 'post.save', 'uses' => 'HomeController@save'));

Route::get('post/{id}/edit', array('as' => 'post.edit', 'uses' => 'HomeController@edit'));
Route::post('post/{id}/update', array('as' => 'post.update', 'uses' => 'HomeController@update'));
Route::get('post/{id}/delete', array('as' => 'post.delete', 'uses' => 'HomeController@delete'));
// Route::get('Home', array('as' => 'home', 'uses' => 'HomeController@index'));
