<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/



    public function index() {
        // $data = array('name' => 'test');
        // $posts = ['posts' => Post::all()->toArray()];
        $posts = ['posts' => Post::all()];
        // dd($posts);
        // return View::make('post.index',['name'=>'test']);
        return View::make('post.index', $posts);
    }

    public function create() {
        return View::make('post.create');
    }

    public function edit($id) {

        $data = ['post' => Post::find($id)];
        return View::make('post.edit', $data);
    }

    public function save() {
        // $data = Input::get();
        Post::postSave(Input::all());
        $posts = ['posts' => Post::all()];
        return View::make('post.index', $posts);
    }

    public function update($id) {
        $data = ['post' => Post::find($id)];
        Post::updatePost($id, Input::all());

        $posts = ['posts' => Post::all()];
        return View::make('post.index', $posts);
        // Post::updatePost(Input::all());
    }

    public function delete($id) {
        $data = ['post' => Post::find($id)];
        Post::deletePost($id);

        // return Redirect::to('home');
        return Redirect::route('home');
    }


    /*

    public function save() {
        $data = Input::get();
        $post = new Post;

        $post->title = Input::get('title');
        $post->body = Input::get('body');
        $post->save();

        // return View::make('post.index');
    }
      */

}
