<?php

class Post extends \Eloquent {
	protected $fillable = [];

    public static function postSave($param) {
        $post = new Post;

        $post->title = $param['title'];
        $post->body = $param['body'];
        $post->save();
    }

    public static function updatePost($id, $param) {
        $post = Post::find($id);

        $post->title = $param['title'];
        $post->body = $param['body'];
        $post->save();
    }

    public static function deletePost($id) {
        $post = Post::find($id);
        $post->delete();
    }
}